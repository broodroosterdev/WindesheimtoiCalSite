$(document).ready(function () {
    const input = document.getElementById("klascode-search");
    const instructions = document.getElementById("instructions");
    var autocomplete;
    var programma;
    var roostertype = "klas";
    $.getJSON("data/" + "klas" + ".json", "", function (data) {
        autocomplete = new Awesomplete(input, {
            list: data.map(data => data.code),
            item: function (suggestion, input) {
                var item = document.createElement("li");
                item.classList.add("list-group-item");
                item.style = "width: 100%";
                var end_suggestion = suggestion.substring(input.length);
                item.innerHTML = "<mark>" + suggestion.substring(0, input
                        .length) +
                    "</mark><b>" + end_suggestion + "</b>";
                return item;
            }
        });
    });
    $("#select-platform").change(function () {
        $(this).find("option:selected").each(function () {
            var optionValue = $(this).attr("value");
            if (optionValue) {
                $("#select-programma option[value='agenda']").each(function () {
                    $(this).remove();
                });
                $("#select-programma option[value='calendar']").each(function () {
                    $(this).remove();
                });
                switch (optionValue) {
                    case "ios":
                        var option = document.createElement('option');
                        option.value = "agenda";
                        option.innerHTML = "Agenda";
                        document.getElementById('select-programma').appendChild(option);
                        break;
                    case "macos":
                        var option = document.createElement('option');
                        option.value = "calendar";
                        option.innerHTML = "Calendar";
                        document.getElementById('select-programma').appendChild(option);
                        break;
                }
                $(".programma-card").show();
                scrollToElement(".programma-card");
            }

        });

    }).change();
    $("#select-programma").change(function () {
        $(this).find("option:selected").each(function () {
            var optionValue = $(this).attr("value");
            if (optionValue) {
                programma = optionValue;
                $(".roostertype-card").show();
                scrollToElement(".roostertype-card");
                
            }
        })
    });
    $('#rooster-type').change(function () {
        $(this).find("option:selected").each(function () {
            var optionValue = $(this).attr("value");
            if (optionValue) {
                loadCodes(optionValue);
                $('.klascode-card').show();
                $("#gaverder").show()
                scrollToBottom();
            }
        })
    })
    $("#gaverder").click(function () {
        $("#rooster-url").val("https://rooster.broodrooster.dev/" + roostertype + "/" + $("#klascode-search").val());
        loadInstructions(programma);
        $("#result").show();
        scrollToBottom();
    })
    $("#copyurl").click(function () {
        copy($("#rooster-url").val());
    })


    function scrollToElement(element){
        $("html, body").animate({ scrollTop: $(element).scrollTop() }, 1000);
    }

    function scrollToBottom(){
        $("html, body").animate({ scrollTop: $(document).height() }, 1000); 
    }

    function copy(text) {
        var aux = document.createElement("input");
        aux.setAttribute("value", text);
        document.body.appendChild(aux);
        aux.select();
        document.execCommand("copy");
        document.body.removeChild(aux);
    }

    function loadInstructions(programmanaam) {
        $.getJSON("data/instructions.json", "", function (data) {
            instructions.innerHTML = data[programmanaam];
        })
    }

    function loadCodes(type) {
        roostertype = type;
        $.getJSON("data/" + type + ".json", "", function (data) {
            autocomplete._list = data.map(data => data.id);
        });
    }
});