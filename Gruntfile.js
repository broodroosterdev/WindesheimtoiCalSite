module.exports = function (grunt) {
    grunt.initConfig({
        uncss: {
            dist: {
                options: {
                    ignore: ['.list-group-item', 'li', 'ul', '.badge-primary', '.badge']
                },
                files: {
                    'public/css/index.css': 'index.html'
                }
            }
        },
        processhtml: {
            dist: {
                files: {
                    'public/index.html': ['index.html']
                }
            }

        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        src: ['data/*'],
                        dest: 'public/',
                        filter: 'isFile'
                    },

                    {
                        expand: true,
                        src: ['js/*'],
                        dest: 'public/',
                        filter: 'isFile'
                    },

                    // includes files within path and its sub-directories
                    {
                        expand: true,
                        src: ['css/fontawesome-free-5.12.1-web/**'],
                        dest: 'public/'
                    },
                ],
            },
        },
    })
    grunt.loadNpmTasks('grunt-uncss');
    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-contrib-copy');

};